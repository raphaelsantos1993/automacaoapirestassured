package core;

import static constants.TimeOutConstants.MAX_TIMEOUT;

import java.util.Base64;
import java.util.LinkedHashMap;

import org.hamcrest.Matchers;

import constants.AuthenticationType;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import model.EnvObject;

public class BaseTest {

	private static String accessTokenField = "access_token";
	static RequestSpecBuilder reqBuild = new RequestSpecBuilder();
	static ResponseSpecBuilder resBuild = new ResponseSpecBuilder();
	public static String envTag = new String();
	public static void initializeRestAssured(String apiName) throws Exception {
		envTag = apiName;
		EnvManager.loadEnvs();
		createRequestSpecification();
		createResponseSpecifications();
		loadToken();
		setRestAssuredSpecifications();
		RestAssured.useRelaxedHTTPSValidation();
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	}

	public static void createRequestSpecification() {
		reqBuild.setContentType(EnvObject.getContent_type());
		reqBuild.setBaseUri(EnvObject.getBase_url());
	}

	public static void createResponseSpecifications() {
		resBuild.expectResponseTime(Matchers.lessThan(MAX_TIMEOUT));
	}

	public static void setRestAssuredSpecifications() {
		if (EnvObject.getHeaders().containsKey(RequestManager.TOKEN_FIELD) || EnvObject.getHeaders().size() > 0) {
			reqBuild.addHeaders(EnvObject.getHeaders());
		}
		RestAssured.requestSpecification = reqBuild.build();
		RestAssured.responseSpecification = resBuild.build();
	}

	public static void loadToken() throws Exception {
		if (!EnvObject.getAuthenticate_url().equals("") && !EnvObject.getAuthenticate_url().isEmpty()) {
			String token = new String();
			String authenticationType = EnvObject.getAuthetication().get("Authentication-Type").toString();
			switch (authenticationType) {
			case AuthenticationType.BASIC_AUTH:
				String basicCredentials = new String(Base64.getEncoder().encode(
						"username:password".getBytes("UTF-8")));
				EnvObject.addHeaders("Authorization", "Basic " + basicCredentials);
				setRestAssuredSpecifications();
				token = RequestManager.getToken();
				EnvObject.removeHeaders("Authorization");
				EnvObject.addHeaders("ACCESS_TOKEN", token);
				break;
			case AuthenticationType.BEARER_TOKEN:
				EnvObject.addHeaders("Authorization", "Bearer " + token);
				token = RequestManager.getToken();
				break;
			}
			System.setProperty(accessTokenField, token);
		}
	}
}
