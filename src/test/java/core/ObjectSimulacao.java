package core;

import org.yaml.snakeyaml.DumperOptions;
import util.CpfGenerator;
import util.EmailGenerator;
import util.NameGenerator;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.SplittableRandom;

public class ObjectSimulacao {
    private String cpf;

    private String nome;

    private String email;

    private Double valor;

    private Integer parcelas;

    private Boolean seguro;

    public void createObject(){
        CpfGenerator cpfGenerator = new CpfGenerator();
        NameGenerator nameGenerator = new NameGenerator();
        EmailGenerator emailGenerator = new EmailGenerator();

        this.cpf = cpfGenerator.cpf(false);
        this.nome = nameGenerator.nameGenarator();
        this.email = emailGenerator.emailGenerator();
        this.valor = randomValue();
        this.parcelas = randomParcela();
        this.seguro = randomSeguro();

    }

    public void putObject(){
        CpfGenerator cpfGenerator = new CpfGenerator();
        NameGenerator nameGenerator = new NameGenerator();
        EmailGenerator emailGenerator = new EmailGenerator();

        this.nome = nameGenerator.nameGenarator();
        this.email = emailGenerator.emailGenerator();
        this.valor = randomValue();
        this.parcelas = randomParcela();
        this.seguro = randomSeguro();

    }

    public void createObjectError(){
        CpfGenerator cpfGenerator = new CpfGenerator();
        NameGenerator nameGenerator = new NameGenerator();
        EmailGenerator emailGenerator = new EmailGenerator();

        this.cpf = "";
        this.nome = nameGenerator.nameGenarator();
        this.email = "teste&com.uk";
        this.valor = randomValue() + 500;
        this.parcelas = randomParcela() -50;
        this.seguro = randomSeguro();

    }


    private double randomValue(){
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        String value= decimalFormat.format(new SplittableRandom().nextInt(1000, 40000));
        return Double.parseDouble(value.replace(",","."));
    }

    private Integer randomParcela(){
       return new SplittableRandom().nextInt(2, 48);
    }

    private Boolean randomSeguro(){
        Random random = new Random();
        return random.nextBoolean();
    }

}
