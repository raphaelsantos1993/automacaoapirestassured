package runner;

import org.junit.runner.RunWith;

import org.junit.runners.Suite;
import steps.SimulacaoCredito;

@RunWith(Suite.class)
@Suite.SuiteClasses(SimulacaoCredito.class)
public class RunTests {
}
