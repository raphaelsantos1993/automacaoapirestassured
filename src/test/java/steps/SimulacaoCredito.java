package steps;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import constants.StatusCode;
import core.BaseTest;
import core.ObjectSimulacao;
import io.restassured.response.Response;
import org.junit.Test;
import util.CpfGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;

public class SimulacaoCredito {

    @Test
    public void getCpfRestritos() throws Exception {
        BaseTest.initializeRestAssured("SimulacaoCredito");
        List<String> cpfRetritos = Arrays.asList("97093236014","60094146012","84809766080","62648716050","26276298085",
                "01317496094","55856777050","19626829001","24094592008", "58063164083");
        for(String item : cpfRetritos) {
            given().basePath("/api/v1/restricoes/" + item).log().all()
                    .when().get()
                    .then().assertThat().statusCode(StatusCode.SUCESS)
                    .assertThat().body("mensagem", equalTo("O CPF "+item+" tem problema"))
                    .log().all();

        }
    }

    @Test
    public void getCpfAllSimulacoes() throws Exception {
        given().basePath("/api/v1/simulacoes")
                    .when().get()
                    .then().assertThat().statusCode(StatusCode.SUCESS).log().all()
                    .extract().response();
            }

    @Test
    public void getCpfSimulacaoPorCpf() throws Exception {
        Response response = given().basePath("/api/v1/simulacoes")
                .when().get();
        given().basePath("/api/v1/simulacoes/" + response.body().jsonPath().get("[0].cpf").toString().replace("\"","")).log().all()
                .when().get()
                .then().assertThat().statusCode(StatusCode.SUCESS)
                .log().all();
    }


    @Test
    public void getCpfSimulacaoPorCpfSemSimulacao() throws Exception {
        CpfGenerator cpfGenerator = new CpfGenerator();
        given().basePath("/api/v1/simulacoes/" + cpfGenerator.cpf(false)).log().all()
                .when().get()
                .then().assertThat().statusCode(StatusCode.NOT_FOUND)
                .log().all();
    }

        @Test
        public void getCpfValido() throws Exception {
            BaseTest.initializeRestAssured("SimulacaoCredito");
            CpfGenerator cpfGenerator = new CpfGenerator();
            String cpf = cpfGenerator.cpf(true);

            List<String> cpfRetritos = Arrays.asList("97093236014","60094146012","84809766080","62648716050","26276298085",
                    "01317496094","55856777050","19626829001","24094592008", "58063164083");
            for(String item : cpfRetritos) {
                if(item == cpf ){
                    cpf = cpfGenerator.cpf(true);
                }
            }

            given().basePath("/api/v1/restricoes/" + cpf).log().all()
                        .when().get()
                        .then().assertThat().statusCode(StatusCode.NO_CONTENT)
                        .log().all();

            }
        @Test
        public void createSimulacaoSucess() throws Exception {
            BaseTest.initializeRestAssured("SimulacaoCredito");
            ObjectSimulacao objectSimulacao = new ObjectSimulacao();
            objectSimulacao.createObject();
            Gson gson = new Gson();
             String json = gson.toJson(objectSimulacao);
             given().basePath("/api/v1/simulacoes")
                    .body(json)
                    .log().all()
                    .when().post()
                    .then().assertThat().statusCode(StatusCode.CREATED)
                    .log().all();
        }

        @Test
        public void createSimulacaoFail() throws Exception {
             BaseTest.initializeRestAssured("SimulacaoCredito");
             ObjectSimulacao objectSimulacao = new ObjectSimulacao();
             objectSimulacao.createObjectError();
             Gson gson = new Gson();
             String json = gson.toJson(objectSimulacao);
             given().basePath("/api/v1/simulacoes")
                .body(json)
                .log().all()
                .when().post()
                .then().assertThat().statusCode(StatusCode.BAD_REQUEST)
                .body("erros.parcelas", is("Parcelas deve ser igual ou maior que 2"))
                .log().all();
    }

    @Test
    public void createSimulacaoCpfRepetido() throws Exception {
        BaseTest.initializeRestAssured("SimulacaoCredito");
        ObjectSimulacao objectSimulacao = new ObjectSimulacao();
        objectSimulacao.createObject();
        Gson gson = new Gson();
        String json = gson.toJson(objectSimulacao);
        given().basePath("/api/v1/simulacoes")
                .body(json)
                .log().all()
                .when().post()
                .then();
        given().basePath("/api/v1/simulacoes")
                .body(json)
                .log().all()
                .when().post()
                .then().assertThat().statusCode(StatusCode.BAD_REQUEST)
                .body("mensagem", is("CPF duplicado"))
                .log().all();
    }

    @Test
    public void updateCpfSimulacaoPorCpf() throws Exception {
        BaseTest.initializeRestAssured("SimulacaoCredito");
        ObjectSimulacao objectSimulacao = new ObjectSimulacao();
        objectSimulacao.putObject();
        Gson gson = new Gson();
        String json = gson.toJson(objectSimulacao);
        Response responseCpf = given().basePath("/api/v1/simulacoes")
                .when().get();

        given().basePath("/api/v1/simulacoes/" + responseCpf.body().jsonPath().get("[0].cpf").toString().replace("\"","")).log().all()
                .body(json)
                .when().put()
                .then().assertThat().statusCode(StatusCode.SUCESS)
                .log().all();

        Response responseAtualizado =  given().basePath("/api/v1/simulacoes/" + responseCpf.body().jsonPath().get("[0].cpf").toString().replace("\"","")).log().all()
                .when().get()
                .then().assertThat().statusCode(StatusCode.SUCESS)
                .log().all().extract().response();
    }

    @Test
    public void deleteAllSimulacoes() throws Exception {
       BaseTest.initializeRestAssured("SimulacaoCredito");
       Response response =  given().basePath("/api/v1/simulacoes")
                .when().get()
                .then().assertThat().statusCode(StatusCode.SUCESS).log().all()
                .extract().response();

        JsonParser jsonParser = new JsonParser();
        JsonArray jArray = jsonParser.parse(response.body().asString()).getAsJsonArray();

       List<String> idList = new ArrayList<>();
       for (int i = 0 ; i < jArray.size() ; i++){
           idList.add(jArray.get(i).getAsJsonObject().get("id").getAsString());
       }

        for(String item : idList) {
            given().basePath("/api/v1/simulacoes/" + item).log().all()
                    .when().delete()
                    .then().assertThat().statusCode(StatusCode.SUCESS)
                    .log().all()
                    .extract().response().body().prettyPrint();

        }
    }

}
