# Simulação de Crédito - Projeto de Automação

Projeto de automação desenvolvido para realização dos testes nos serviços de simulação de crédito.

##  Requisitos para execução dos testes
 * Java 8+ JDK deve estar instalado
 * IDE compátivel com a linguagem java (ex: IntelliJ ou Eclipse)
 * O ambiente da aplicação deve estar em execução
 
## Como executar os testes

Pela IDE deve ser executada a classe runner RunTests.java que se encontra no caminho: **src/test/java/runners/RunTests.java**

Pelo terminal/CMD deve acessar o diretório raíz do projeto e utilizar o comando maven para execução dos testes:
```bash
mvn test -Dtest=RunTests
```

## Documentacão técnica
### Diretórios
**Features:** src/test/resources/features
**Arquivo de centralzação de dados (env.yaml):** src/test/resources/env.yaml
**Classe executora de testes (RunTests.java):** src/test/java/runners/RunTests.java
**Diretório de armazenamento dos arquivos utilizados como payload (Fixtures)**: src/test/resources/features
**Classe responsável por centralizar as requisições REST (RequestManager.java)**: src/test/java/core/RequestManager.java

### Pacotes
##### constants
Possui classes para centralizar dados em constantes utilizados durante a execução por exemplo os códigos de status das requisições.

Contem as seguintes classes:
**AuthenticationType.java**
**StatusCode.java**
**PathConstants.java**
**TimeOutConstants.java**

##### core
Possui classes de estruturação dos frameworks utilizados durante o desenvolvimento e execução dos scripts.

Contem as seguintes classes:
**BaseTest.java**
**EnvManager.java**
**ReadEnvs.java**
**ObjectSimulação.java**
**RequestManager.java**

##### model
Possui classes utilizadas como modelo de dados durante as execuções dos testes.

Contem as seguintes classes:
**EnvObject.java**
**Response.java**

##### runners
Possui classes utilizdas como executora dos testes automatizados
Contem as seguintes classes:
**RunTests.java**

##### steps
Possui classes utilizadas para implementação das requisições e validação dos testes.

Contem as seguintes classes:
**SimulaçãoCredito.java**

##### util
Possui classes auxiliares utilizadas durante o desenvolvimento dos scripts automatizados e possui funcionalidades como:
-Geração de CPFs válidos
-Geração de Emails válidos
-Geração de nomes
-Manipulação de arquivos Json
-Manipulação de arquivos yaml
-Manipulação de Strings

Contem as seguintes classes:
**CpfGenerator.java**
**EmailGenerator.java**
**FileManager.java**
**JsonManager.java**
**NameGenerator.java**
**StringManager.java**
**YamlManager.java**











